import logo from "./logo.svg";
import "./App.css";
import BtForm from "./BTForm/BtForm";

function App() {
  return (
    <div>
      <BtForm />
    </div>
  );
}

export default App;
