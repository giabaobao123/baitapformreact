import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../Store/slice";
const FormQuanLys = () => {
  const [formData, setForm] = useState();
  const [error, setEror] = useState();
  const { listSinhVien, editing } = useSelector((state) => state.btFormToolKit);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!editing) return;
    setForm(editing);
  }, [editing]);
  const handelChange = (e) => {
    const { name, value, pattern } = e.target;
    const Regex = new RegExp(pattern);
    const maxLength = 50;
    const minLength = 5;
    let mess;
    if (!value.length) {
      mess = "*Không để trống ô này";
    } else if (name == "ma" && !Regex.test(value)) {
      mess = `*Mã sinh viên phải là số`;
    } else if (name == "phone" && !Regex.test(value)) {
      mess = "*Số điện thoại phải thuộc mã vùng Việt Nam";
    } else if (name == "email" && !Regex.test(value)) {
      mess = "*Email chưa đúng định dạng";
    } else if (value.length > maxLength) {
      mess = `*Không được nhập quá ${maxLength} ký tự`;
    } else if (value.length < minLength) {
      mess = `*Không nhập ít hơn ${minLength} ký tự`;
    } else if (name == "ma" && listSinhVien.find((e) => e.ma == value)) {
      mess = `Tên id không được trùng`;
    }
    setEror({
      ...error,
      [name]: mess,
    });
    setForm({
      ...formData,
      [name]: mess ? undefined : value,
    });
  };
  return (
    <div>
      <form
        noValidate
        action=""
        onSubmit={(e) => {
          e.preventDefault();
          let Allinput = document.querySelectorAll(".form");
          let error = {};
          Allinput.forEach((input) => {
            let mess;
            const { name, value, pattern } = input;
            const Regex = new RegExp(pattern);
            const maxLength = 50;
            const minLength = 5;
            if (!value.length) {
              mess = "*Không để trống ô này";
            } else if (name == "ma" && !Regex.test(value)) {
              mess = `*Mã sinh viên phải là số`;
            } else if (name == "phone" && !Regex.test(value)) {
              mess = "*Số điện thoại phải thuộc mã vùng Việt Nam";
            } else if (name == "email" && !Regex.test(value)) {
              mess = "*Email chưa đúng định dạng";
            } else if (value.length > maxLength) {
              mess = `*Không được nhập quá ${maxLength} ký tự`;
            } else if (value.length < minLength) {
              mess = `*Không nhập ít hơn ${minLength} ký tự`;
            } else if (
              name == "ma" &&
              listSinhVien.find((e) => e.ma == value)
            ) {
              mess = `Tên id không được trùng`;
            }
            error[name] = mess;
          });
          setEror(error);
          let flag = false;
          for (const key in error) {
            if (error[key]) {
              flag = true;
              return;
            }
          }
          if (flag) {
            return;
          }
          dispatch(btFormAction.addSinhVien(formData));
          setForm({
            ma: "",
            name: "",
            phone: "",
            email: "",
          });
        }}
      >
        <div className="row">
          <div className="col-6">
            <p>Mã SV</p>
            <input
              disabled={editing}
              value={formData?.ma}
              pattern="^[0-9]+$"
              type="text"
              className="form-control form"
              name="ma"
              onChange={handelChange}
            />
            <p className="text-danger">{error?.ma}</p>
          </div>
          <div className="col-6">
            <p>Họ tên</p>
            <input
              value={formData?.name}
              type="text"
              className="form-control form"
              name="name"
              onChange={handelChange}
            />
            <p className="text-danger">{error?.name}</p>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-6">
            <p>Số điện thoại</p>
            <input
              value={formData?.phone}
              pattern="(84|0[3|5|7|8|9])+([0-9]{8})\b"
              type="text"
              className="form-control form"
              name="phone"
              onChange={handelChange}
            />
            <p className="text-danger">{error?.phone}</p>
          </div>
          <div className="col-6">
            <p>Email</p>
            <input
              value={formData?.email}
              pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
              type="text"
              className="form-control form"
              name="email"
              onChange={handelChange}
            />
            <p className="text-danger">{error?.email}</p>
          </div>
        </div>
        <div className="mt-4">
          {
            <button
              className=" mr-3 btn btn-success "
              type="submit"
              disabled={editing}
            >
              Thêm sinh viên
            </button>
          }
          {
            <button
              className="btn btn-info"
              type="button"
              disabled={!editing}
              onClick={() => {
                let Allinput = document.querySelectorAll(".form");
                let error = {};
                Allinput.forEach((input) => {
                  let mess;
                  const { name, value, pattern } = input;
                  const Regex = new RegExp(pattern);
                  const maxLength = 50;
                  const minLength = 5;
                  if (!value.length) {
                    mess = "*Không để trống ô này";
                  } else if (name == "ma" && !Regex.test(value)) {
                    mess = `*Mã sinh viên phải là số`;
                  } else if (name == "phone" && !Regex.test(value)) {
                    mess = "*Số điện thoại tồn tại ở việt nam";
                  } else if (name == "email" && !Regex.test(value)) {
                    mess = "*Email chưa đúng định dạng";
                  } else if (value.length > maxLength) {
                    mess = `*Không được nhập quá ${maxLength} ký tự`;
                  } else if (value.length < minLength) {
                    mess = `*Không nhập ít hơn ${minLength} ký tự`;
                  }
                  error[name] = mess;
                });
                setEror(error);

                let flag = false;
                for (const key in error) {
                  if (error[key]) {
                    flag = true;
                    return;
                  }
                }
                if (flag) {
                  return;
                }
                dispatch(btFormAction.updatingSinhVien(formData));
                setForm({
                  ma: "",
                  name: "",
                  phone: "",
                  email: "",
                });
                dispatch(btFormAction.isEditing());
              }}
            >
              Cập nhật Thông Tin
            </button>
          }
        </div>
      </form>
    </div>
  );
};

export default FormQuanLys;
