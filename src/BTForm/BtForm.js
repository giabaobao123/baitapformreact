import React from "react";
import FormQuanLys from "./FormQuanLys";
import TableForm from "./TableForm";
import "../Css/style.css";
const BtForm = () => {
  return (
    <div>
      <div className="container mt-5">
        <h1 className="bg-dark text-light p-3">Thông tin sinh viên</h1>
        <FormQuanLys />
        <TableForm />
      </div>
    </div>
  );
};

export default BtForm;
