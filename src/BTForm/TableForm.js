import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../Store/slice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoffee, faSearch } from "@fortawesome/free-solid-svg-icons";

const TableForm = () => {
  const { listSinhVien } = useSelector((state) => state.btFormToolKit);
  const [filter, setFilter] = useState();
  const dispatch = useDispatch();
  // filter giá trị tìm kiếm
  const handelChange = (e) => {
    const { value } = e.target;
    if (value == "") {
      setFilter();
      return;
    }
    let listMa = listSinhVien.filter((sv) => sv.ma.includes(value));
    setFilter(listMa);
  };
  const changeList = () => {
    if (!filter) {
      return listSinhVien;
    }
    return filter;
  };

  return (
    <div className="mt-3">
      <div className="">
        <input
          onChange={handelChange}
          type="text"
          className="mt-3 w-50 mb-3 form-control "
          placeholder="Search for id...."
        />
      </div>

      <table className="table" border={1}>
        <thead className="bg-dark text-light">
          <th>Mã SV</th>
          <th>Tên Họ tên</th>
          <th>Số điện thoại</th>
          <th>Email</th>
          <th>Action</th>
        </thead>
        <tbody>
          {changeList()?.map((sv) => {
            return (
              <tr>
                <td>{sv?.ma}</td>
                <td>{sv?.name}</td>
                <td>{sv?.phone}</td>
                <td>{sv?.email}</td>
                <td>
                  <button
                    className="btn btn-danger mr-3"
                    onClick={() => dispatch(btFormAction.deleteSinhVien(sv.ma))}
                  >
                    Delete
                  </button>
                  <button
                    className="btn btn-warning ml-3"
                    onClick={() => {
                      dispatch(btFormAction.editingSinhVien(sv.ma));
                    }}
                  >
                    Edit
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default TableForm;
