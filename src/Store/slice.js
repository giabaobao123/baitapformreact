import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  listSinhVien: [],
  editing: undefined,
  listName: undefined,
};
export const btForm = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addSinhVien(state, action) {
      state.listSinhVien.push(action.payload);
    },
    deleteSinhVien(state, action) {
      state.listSinhVien = state.listSinhVien.filter(
        (sv) => sv.ma !== action.payload
      );
    },
    isEditing(state, action) {
      state.editing = undefined;
    },
    editingSinhVien(state, action) {
      let index = state.listSinhVien.findIndex((e) => e.ma == action.payload);
      state.editing = state.listSinhVien[index];
    },
    updatingSinhVien(state, action) {
      let index = state.listSinhVien.findIndex(
        (e) => e.ma == action.payload.ma
      );
      state.listSinhVien[index] = action.payload;
    },
  },
});
export const { actions: btFormAction, reducer: btFormReducer } = btForm;
