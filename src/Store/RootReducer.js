import { combineReducers } from "redux";
import { btFormReducer } from "./slice";

export const RootReducer = combineReducers({
  btFormToolKit: btFormReducer,
});
